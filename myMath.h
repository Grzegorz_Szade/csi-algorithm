#pragma once
#include <random>
#include <assert.h>
class MyMath
{
public:
	template <class T>
	static T absolute(T t);

	template <class T>
	static T random();

	template <class T>
	static T random(int from, int to);

	//0 to 1
	template <class T>
	static T random1();

	template <class T>
	static T pow(T x, int n);
};

template<class T>
T MyMath::absolute(T t)
{
	return t < (T)0 ? t * (T)-1 : t;
}

//to be compatible with older excercises
template<class T>
T MyMath::random()
{
	std::random_device rd;
	std::default_random_engine generator(rd());
	std::uniform_int_distribution<int> distribution(-65536, 65535)
	T ret;
	ret = distribution(generator);
	ret = ret / (T)65536;
	return ret;
}

template<class T>
T MyMath::random(int from, int to)
{
	assert(from <= to);
	std::random_device rd;
	std::default_random_engine generator(rd());
	std::uniform_int_distribution<int> distribution(from, to);
	T ret;
	ret = distribution(generator);
	return ret;
}

template<class T>
inline T MyMath::random1()
{
	std::random_device rd;
	std::default_random_engine generator(rd());
	std::uniform_int_distribution<int> distribution(10000, 1000000000);
		T ret;
	ret = distribution(generator);
	ret = ret / (T)1000000000;
	return ret;
}

template<class T>
T MyMath::pow(T x, int n)
{
	assert(n >= 1);
	T ret_val = x;
	for (int i = 2; i <= n; i++)
	{
		ret_val *= x;
	}
	return ret_val;
}
