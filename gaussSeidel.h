#pragma once
#include "myMatrix.h"
#include "myVector.h"

class GaussSeidel
{
public:
	static MyVector<double> * gaussSeidel(MyMatrix<double> &a, MyVector<double> &b, int iter);
};