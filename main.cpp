#include <iostream>
#include "gaussCalculation.h"
#include "jacobi.h"
#include "sparse.h"
#include "data.h"
#include "interpolatingFunction.h"
#include "CSI.h"
#include "CSV-parser.h"
#include "gaussSeidel.h"
#include "mapsAPIParser.h"
#include <chrono>

void drawGraphLow(Data * data, InterpolatingFunction * fn, int nodes_count, double accuracy, CSVParser &csv, const char * filename)
{
	csv.clear(filename);
	csv.open(filename);

	int j = 0;
	for (double x = 0.0; x < data[nodes_count - 1].distance; x += accuracy)
	{
		if (j < nodes_count - 2)
			if (x > fn[j + 1].xj)
				j++;
		csv.addElement(x);
		csv.addElement(fn[j].execute(x));
		csv.addLine();
	}

	csv.close();
}

void drawGraph(const char * filename_src, const char * filename_dest, MethodEnum method, double accuracy, int every, CSVParser &csv)
{
	int nodes_count;
	Data * data = MapsAPIParser::loadData(filename_src, every, &nodes_count);

	InterpolatingFunction * fn = NULL;
	fn = CSI::csiMethod(data, nodes_count - 1, method);

	drawGraphLow(data, fn, nodes_count, accuracy, csv, filename_dest);
	delete [] data;
	delete[] fn;
}


double countTime(Data * data, int n, MethodEnum method)
{
	InterpolatingFunction * fn = NULL;
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	fn = CSI::csiMethod(data, n, method);
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	delete[] fn;
	return std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();
}

void e1()
{
	CSVParser csv;
	csv.clear("E1.csv");
	csv.open("E1.csv");

	csv.addElement("Nodes");
	csv.addElement("Gauss");
	csv.addElement("Gauss-Seidel");
	csv.addElement("Jacobi");
	csv.addElement("Jacobi_Sparse");
	csv.addElement("EIGEN_Sparse");
	csv.addLine();

	Data * data = NULL;

	for (int for_nodes = 10; for_nodes < 201; for_nodes += 1)
	{
		int nodes_count;
		data = MapsAPIParser::loadData("gory.txt", 1, &nodes_count, for_nodes);

		csv.addElement(for_nodes);
		double duration = 0.0;
		duration = countTime(data, nodes_count - 1, METHOD_GAUSS);
		csv.addElement(duration);
		std::cout << "GAUSS: " << duration << " for" << for_nodes << " nodes" << std::endl;
		duration = countTime(data, nodes_count - 1, METHOD_GAUSS_SEIDEL);
		csv.addElement(duration);
		std::cout << "GAUSS-SEIDEL: " << duration << " for" << for_nodes << " nodes" << std::endl;
		duration = countTime(data, nodes_count - 1, METHOD_JACOBI);
		csv.addElement(duration);
		std::cout << "JACOBI: " << duration << " for" << for_nodes << " nodes" << std::endl;
		duration = countTime(data, nodes_count - 1, METHOD_JACOBI_SPARSE);
		csv.addElement(duration);
		std::cout << "JACOBI_SPARSE: " << duration << " for" << for_nodes << " nodes" << std::endl;
		duration = countTime(data, nodes_count - 1, METHOD_EIGEN_SPARSE);
		csv.addElement(duration);
		std::cout << "EIGEN_SPARSE: " << duration << " for" << for_nodes << " nodes" << std::endl;

		csv.addLine();
		delete [] data;
	}
	csv.close();
}

double avgAbsoluteError(const char * filename, int every, MethodEnum method)
{
	int nodes_count_full;
	Data * datafull = MapsAPIParser::loadData(filename, 1, &nodes_count_full);

	int nodes_count_every4;
	Data * dataEvery4 = MapsAPIParser::loadData(filename, every, &nodes_count_every4);

	InterpolatingFunction * fnEvery4 = NULL;
	fnEvery4 = CSI::csiMethod(dataEvery4, nodes_count_every4 - 1, method);

	double total_of_absolute_error = 0.0;
	int counter = 0;
	int j = 0;
	for (int data_iterator = every / 2; data_iterator < nodes_count_every4; data_iterator += every)
	{

		while (!(datafull[data_iterator].distance > fnEvery4[j].xj && datafull[data_iterator].distance < fnEvery4[j + 1].xj))
		{

			j++;
		}
		double absolute_error = abs(datafull[data_iterator].height - fnEvery4[j].execute(datafull[data_iterator].distance));


		total_of_absolute_error += absolute_error;

		counter++;


	}
	double avg_of_absolute_error = total_of_absolute_error / counter;


	delete[] datafull;
	delete[] dataEvery4;
	delete[] fnEvery4;
	return avg_of_absolute_error;
}


void c1()
{
	CSVParser csv;
	csv.clear("C1.csv");
	csv.open("C1.csv");
	int every = 4;
	csv.addElement("Gauss");
	csv.addElement(avgAbsoluteError("gory.txt", every, METHOD_GAUSS));
	csv.addLine();
	csv.addElement("Gauss-Seidel");
	csv.addElement(avgAbsoluteError("gory.txt", every, METHOD_GAUSS_SEIDEL));
	csv.addLine();
	csv.addElement("Jacobi");
	csv.addElement(avgAbsoluteError("gory.txt", every, METHOD_JACOBI));
	csv.addLine();
	csv.addElement("Jacobi-sparse");
	csv.addElement(avgAbsoluteError("gory.txt", every, METHOD_JACOBI_SPARSE));
	csv.addLine();
	csv.addElement("Eigen-sparse");
	csv.addElement(avgAbsoluteError("gory.txt", every, METHOD_EIGEN_SPARSE));
	csv.addLine();

	drawGraph("gory.txt", "teren-gorzysty-full.csv", METHOD_GAUSS, 1.0, 1, csv);
	drawGraph("gory.txt", "teren-gorzysty-every10.csv", METHOD_GAUSS, 1.0, 10, csv);

	drawGraph("pustynia.txt", "teren-pustynny-full.csv", METHOD_GAUSS, 1.0, 1, csv);
	drawGraph("pustynia.txt", "teren-pustynny-every10.csv", METHOD_GAUSS, 1.0, 10, csv);
	csv.close();
}

void c2()
{
	CSVParser csv;
	csv.clear("C2.csv");
	csv.open("C2.csv");
	csv.addElement("(empty)");
	csv.addElement("a");
	csv.addElement("b");
	csv.addElement("c");
	csv.addElement("d");
	csv.addLine();

	int nodes_count;
	Data * data = MapsAPIParser::loadData("gory.txt", 1.0, &nodes_count);
	InterpolatingFunction * fn = NULL;

	for (int i = 0; i < 5; i++)
	{
		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_GAUSS);
		csv.addElement("Gauss");
		csv.addElement(fn[i].a);
		csv.addElement(fn[i].b);
		csv.addElement(fn[i].c);
		csv.addElement(fn[i].d);
		csv.addLine();
		delete[] fn;

		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_GAUSS_SEIDEL);
		csv.addElement("Gauss-Seidel");
		csv.addElement(fn[i].a);
		csv.addElement(fn[i].b);
		csv.addElement(fn[i].c);
		csv.addElement(fn[i].d);
		csv.addLine();
		delete[] fn;

		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_JACOBI);
		csv.addElement("Jacobi");
		csv.addElement(fn[i].a);
		csv.addElement(fn[i].b);
		csv.addElement(fn[i].c);
		csv.addElement(fn[i].d);
		csv.addLine();
		delete[] fn;

		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_JACOBI_SPARSE);
		csv.addElement("Jacobi Sparse");
		csv.addElement(fn[i].a);
		csv.addElement(fn[i].b);
		csv.addElement(fn[i].c);
		csv.addElement(fn[i].d);
		csv.addLine();
		delete[] fn;

		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_EIGEN_SPARSE);
		csv.addElement("Eigen Sparse");
		csv.addElement(fn[i].a);
		csv.addElement(fn[i].b);
		csv.addElement(fn[i].c);
		csv.addElement(fn[i].d);
		csv.addLine();
		csv.addLine();
		delete[] fn;
	}
	

	delete[] data;
}

void e3()
{
	CSVParser csv;
	csv.clear("e3.csv");
	csv.open("e3.csv");

	csv.addElement("iter");
	csv.addElement("Jacobi");
	csv.addElement("Gauss-Seidel");
	csv.addLine();

	int nodes_count;
	double duration;
	Data * data = MapsAPIParser::loadData("gory.txt", 1, &nodes_count);
	std::chrono::steady_clock::time_point begin;
	std::chrono::steady_clock::time_point end;
	InterpolatingFunction * fn = NULL;
	for (int iter = 2; iter < 30; iter++)
	{
		csv.addElement(iter);

		begin = std::chrono::steady_clock::now();
		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_JACOBI, iter);
		end = std::chrono::steady_clock::now();

		duration = std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();
		csv.addElement(duration);

		begin = std::chrono::steady_clock::now();
		fn = CSI::csiMethod(data, nodes_count - 1, METHOD_GAUSS_SEIDEL, iter);
		end = std::chrono::steady_clock::now();

		duration = std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();
		csv.addElement(duration);
		csv.addLine();
	}
}

int main()
{
	///////////////////////////
	////GAUSS N JACOBI  TEST
	////gauss test. works.
	//MyMatrix<double> a_u2(3, 3);
	//a_u2.m[0][0] = 21;
	//a_u2.m[1][0] = 1;
	//a_u2.m[2][0] = 0;
	//a_u2.m[0][1] = 1;
	//a_u2.m[1][1] = 32;
	//a_u2.m[2][1] = 1;
	//a_u2.m[0][2] = 0;
	//a_u2.m[1][2] = 1;
	//a_u2.m[2][2] = 64;
	//MyVector<double> v_u2(3);
	//v_u2.values[0] = 1.0 / 7654.0;
	//v_u2.values[1] = 1.0 / 2376.0;
	//v_u2.values[2] = 1.0 / 903.0;

	////solution:
	////4.83999
	////-6.4555
	////9.88235
	//MyVector<double> * solution1 = NULL;
	//solution1 = GaussCalculation::gaussPartiallyChosenElement(&a_u2, &v_u2);
	////a_u2.print();
	////v_u2.print();
	//solution1->print();

	//MyVector<double> * solution2 = NULL;
	////solution2->print();
	//solution2 = Jacobi::jacobi(a_u2, v_u2, 10);
	//solution2->print();


	//MyVector<double> * solution3 = NULL;
	////solution3.print();
	//Sparse s(&a_u2);
	////s.print();
	//solution3 = Jacobi::jacobiSprase(s, v_u2, 10);
	//solution3->print();

	//MyVector<double> * solution4 = NULL;
	////solution2->print();
	//solution4 = GaussSeidel::gaussSeidel(a_u2, v_u2, 10);
	//solution4->print();

	////GAUSS N JACOBI TEST
	/////////////////////////////

	////load data
	//int NODES_COUNT = 3;
	//Data * data = new Data[NODES_COUNT];
	//data[0].distance = 0.0;
	//data[0].height = 20.0;
	//data[1].distance = 20.0;
	//data[1].height = 50.0;
	//data[2].distance = 70.0;
	//data[2].height = 45.0;

	//load data
	//int nodes_count;
	//Data * data = MapsAPIParser::loadData("gory.txt", 2, &nodes_count);


	//int nodes_count;
	//Data * data = MapsAPIParser::loadData("gory.txt", 1, &nodes_count, 20);



	////create fns
	//InterpolatingFunction * fn = NULL;
	//fn = CSI::csiMethod(data, nodes_count - 1, METHOD_EIGEN_SPARSE);

	////draw fns
	//CSVParser csv;
	//drawGraph(data, fn, nodes_count, 1.0, csv, "E1.csv");


	//STATISTICS:
	//e1();

	//C1:
	
	//c1();
	//c2();

	e3();
	system("pause");
	return 0;
}