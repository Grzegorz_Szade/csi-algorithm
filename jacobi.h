#pragma once
#include "myMatrix.h"
#include "myVector.h"
#include "sparse.h"

class Jacobi
{
public:
	static MyVector<double> * jacobi(MyMatrix<double> &a, MyVector<double> &b, int iter);
	static MyVector<double> * jacobiSprase(Sparse &a, MyVector<double> &b, int iter);
};