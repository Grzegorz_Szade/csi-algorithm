#pragma once
#include "myMatrix.h"
#include "errorHandler.h"

typedef struct _SparseElement
{
	int i;
	int j;
	double value;
} SparseElement;

class Sparse
{
public:
	int source_matrix_size;
	int elements_count;
	SparseElement * m;

	Sparse(const MyMatrix<double> * m);
	Sparse(int matrix_size);
	~Sparse();

	double getValue(int i, int j);
	void setValue(int i, int j, double value);
	void print();
};