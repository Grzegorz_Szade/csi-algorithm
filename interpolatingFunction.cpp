#include "interpolatingFunction.h"

InterpolatingFunction::InterpolatingFunction() : a(0.0), b(0.0), c(0.0), d(0.0), xj(0.0) {}

double InterpolatingFunction::execute(double x)
{
	return a + (b * (x - xj)) + (c * pow(x - xj, 2)) + (d * pow(x - xj, 3));
}
