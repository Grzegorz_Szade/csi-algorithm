#include "CSI.h"

InterpolatingFunction * CSI::csiMethod(Data * data, int n, MethodEnum method, int iter)
{
	if (method == METHOD_EIGEN_SPARSE)
		return eigenCsiMethod(data, n);

	MyVector<double> delta(n + 1);
	MyVector<double> * m = NULL;

	MyVector<double> mi(n + 1);
	MyVector<double> lambda(n + 1);

	lambda.values[0] = 0.0;
	delta.values[0] = 0.0;

	for (int j = 1; j < n; j++)
	{
		double hFormer = data[j].distance - data[j - 1].distance;
		double hLatter = data[j + 1].distance - data[j].distance;

		mi.values[j] = hFormer / (hFormer + hLatter);
		lambda.values[j] = hLatter / (hFormer + hLatter);
		delta.values[j] = (6 / (hFormer + hLatter)) * (((data[j + 1].height - data[j].height) / hLatter) - ((data[j].height - data[j - 1].height) / hFormer));
	}

	mi.values[n] = 0.0;
	delta.values[n] = 0.0;

	if (method == METHOD_GAUSS || method == METHOD_GAUSS_SEIDEL || method == METHOD_JACOBI)
	{
		//normal Matrix initialization
		MyMatrix<double> matrix(n + 1);
		for (int i = 0; i < matrix.sizeX; i++)
		{
			for (int j = 0; j < matrix.sizeX; j++)
			{
				if (i == j)
				{
					matrix.m[i][j] = 2.0;
				}
				//left side
				else if (i - j == 1)
				{
					matrix.m[i][j] = mi.values[i];
				}
				//right side
				else if (i - j == -1)
				{
					matrix.m[i][j] = lambda.values[i];
				}
			}
		}
		if (method == METHOD_GAUSS)
			m = GaussCalculation::gaussNoElement(&matrix, &delta);
		else if (method == METHOD_JACOBI)
			m = Jacobi::jacobi(matrix, delta, iter);
		else if (method == METHOD_GAUSS_SEIDEL)
			m = GaussSeidel::gaussSeidel(matrix, delta, iter);
	}
	else if (method == METHOD_JACOBI_SPARSE)
	{
		Sparse matrix(n + 1);
		matrix.setValue(0, 0, 2.0);
		matrix.setValue(0, 1, 0.0);
		for (int i = 1; i < matrix.source_matrix_size - 1; i++)
		{
			matrix.setValue(i, i, 2.0);
			//right side
			matrix.setValue(i, i + 1, lambda.values[i]);
			//left side
			matrix.setValue(i, i - 1, mi.values[i]);
		}
		matrix.setValue(n, n - 1, 0.0);
		matrix.setValue(n, n, 2.0);
		m = Jacobi::jacobiSprase(matrix, delta, 10);
	}

	//Creating functions
	InterpolatingFunction * fn = new InterpolatingFunction[n];
	for (int j = 0; j < n; j++)
	{
		double hLatter = data[j + 1].distance - data[j].distance;
		fn[j].a = data[j].height;
		fn[j].b = ((data[j + 1].height - data[j].height) / hLatter) - ((((2 * m->values[j]) + m->values[j + 1]) / 6) * hLatter);
		fn[j].c = m->values[j] / 2;
		fn[j].d = (m->values[j + 1] - m->values[j]) / (6 * hLatter);
		fn[j].xj = data[j].distance;
	}

	delete m;
	return fn;
}

InterpolatingFunction * CSI::eigenCsiMethod(Data * data, int n)
{
	Eigen::VectorXd delta(n + 1);
	Eigen::VectorXd m(n + 1);

	Eigen::VectorXd mi(n + 1);
	Eigen::VectorXd lambda(n + 1);

	lambda[0] = 0.0;
	delta[0] = 0.0;

	for (int j = 1; j < n; j++)
	{
		double hFormer = data[j].distance - data[j - 1].distance;
		double hLatter = data[j + 1].distance - data[j].distance;

		mi[j] = hFormer / (hFormer + hLatter);
		lambda[j] = hLatter / (hFormer + hLatter);
		delta[j] = (6 / (hFormer + hLatter)) * (((data[j + 1].height - data[j].height) / hLatter) - ((data[j].height - data[j - 1].height) / hFormer));
	}

	mi[n] = 0.0;
	delta[n] = 0.0;


	Eigen::SparseMatrix<double> matrix(n + 1, n + 1);
	Coefficients coefficients;

	coefficients.addElement(0, 0, 2.0);
	coefficients.addElement(0, 0, 2.0);
	coefficients.addElement(0, 1, 0.0);
	for (int i = 1; i < n; i++) //?
	{
		coefficients.addElement(i, i, 2.0);
		//right side
		coefficients.addElement(i, i + 1, lambda[i]);
		//left side
		coefficients.addElement(i, i - 1, mi[i]);
	}
	coefficients.addElement(n, n - 1, 0.0);
	coefficients.addElement(n, n, 2.0);
	matrix.setFromTriplets(coefficients.getVector()->begin(), coefficients.getVector()->end());
	Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;
	solver.analyzePattern(matrix);
	solver.factorize(matrix);
	m = solver.solve(delta);

	//Creating functions
	InterpolatingFunction * fn = new InterpolatingFunction[n];
	for (int j = 0; j < n; j++)
	{
		double hLatter = data[j + 1].distance - data[j].distance;
		fn[j].a = data[j].height;
		fn[j].b = ((data[j + 1].height - data[j].height) / hLatter) - ((((2 * m[j]) + m[j + 1]) / 6) * hLatter);
		fn[j].c = m[j] / 2;
		fn[j].d = (m[j + 1] - m[j]) / (6 * hLatter);
		fn[j].xj = data[j].distance;
	}
	return fn;
}