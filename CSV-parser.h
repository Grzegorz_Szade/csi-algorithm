#ifndef CSV_PARSER_H
#define CSV_PARSER_H

#include <fstream>
#include <string.h>

class CSVParser
{
private:
	std::fstream *m_file;

public:
	CSVParser();
	CSVParser(CSVParser &other) = delete;
	~CSVParser();

	bool open(const char * filename);

	void addElement(double element);
	void addElement(int element);
	void addElement(long long element);
	void addElement(std::string element);

	void addLine();

	void clear(const char * filename);

	void close();

};
#endif //CSV_PARSER_H