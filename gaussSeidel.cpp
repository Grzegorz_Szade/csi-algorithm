#include "gaussSeidel.h"

MyVector<double>* GaussSeidel::gaussSeidel(MyMatrix<double>& a, MyVector<double>& b, int iter)
{
	MyVector<double> * ret_x = new MyVector<double>(b.size);
	for (int i = 0; i < iter; i++)
	{
		for (int w = 0; w < ret_x->size; w++)
		{
			double x0 = 0;
			for (int k = 0; k < ret_x->size; k++)
			{
				if (w != k)
					x0 += a.m[w][k] * ret_x->values[k];
			}
			ret_x->values[w] = (b.values[w] - x0) / a.m[w][w];
		}
	}
	return ret_x;
}