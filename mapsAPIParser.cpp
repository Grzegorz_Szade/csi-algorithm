#include "mapsApiParser.h"

int MapsAPIParser::countData(const char * filename, int every)
{
	assert(every > 0);
	std::fstream file;
	file.open(filename);
	if (file.fail())
	{
		std::cerr << "Error" << std::endl;
	}
	int counter = 0;
	double distance;
	double height;
	char seperator;
	int every_counter = 0;
	while (file >> distance >> seperator >> height)
	{
		if(every_counter++ % every == 0)
			counter++;
	}
	return counter;
}

Data * MapsAPIParser::loadData(const char * filename, int every, int * nodes_count_dest)
{
	int nodes_count = countData(filename, every);
	Data * ret_data = new Data[nodes_count];
	std::fstream file;
	file.open(filename);
	if (file.fail())
	{
		std::cerr << "Error" << std::endl;
	}

	double distance;
	double height;
	char seperator;

	int counter = 0;
	int every_counter = 0;
	while (file >> distance >> seperator >> height)
	{
		if (every_counter++ % every == 0)
		{
			ret_data[counter].distance = distance;
			ret_data[counter].height = height;
			counter++;
		}
	}

	*nodes_count_dest = nodes_count;
	return ret_data;
}

Data * MapsAPIParser::loadData(const char * filename, int every, int * nodes_count_dest, int firstXnodes)
{
	int nodes_count = firstXnodes;
	Data * ret_data = new Data[nodes_count];
	std::fstream file;
	file.open(filename);
	if (file.fail())
	{
		std::cerr << "Error" << std::endl;
	}

	double distance;
	double height;
	char seperator;

	for (int i = 0; i < firstXnodes; i++)
	{
		file >> distance >> seperator >> height;
		ret_data[i].distance = distance;
		ret_data[i].height = height;
	}
	////to test if work properly
	//for (int i = 0; i < nodes_count; i++)
	//{
	//	std::cout.precision(20);
	//	std::cout << "punkt :" << ret_data[i].distance << "  " << ret_data[i].height << std::endl;
	//}

	*nodes_count_dest = nodes_count;
	return ret_data;
}
