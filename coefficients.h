#pragma once
#include <vector>
#include "Eigen\Sparse"

class Coefficients
{
	std::vector<Eigen::Triplet<double>> coefficients;
public:
	std::vector<Eigen::Triplet<double>> * getVector();
	void addElement(int i, int j, double value);
};