#include "sparse.h"

Sparse::Sparse(const MyMatrix<double>* matrix) : Sparse(matrix->sizeX)
{
	assert(matrix->sizeX == matrix->sizeY);
	int iter = 0;
	for (int i = 0; i < source_matrix_size; i++)
	{
		for (int j = 0; j < source_matrix_size; j++)
		{
			if (i == j || i - j == 1 || j - i == 1)
			{
				m[iter].i = i;
				m[iter].j = j;
				m[iter].value = matrix->m[i][j];
				iter++;
			}
		}
	}

}

Sparse::Sparse(int matrix_size)
{
	source_matrix_size = matrix_size;
	elements_count = source_matrix_size * 3 - 2;
	m = new SparseElement[elements_count];

	int iter = 0;
	for (int i = 0; i < source_matrix_size; i++)
	{
		for (int j = 0; j < source_matrix_size; j++)
		{
			if (i == j || i - j == 1 || j - i == 1)
			{
				m[iter].i = i;
				m[iter].j = j;
				m[iter].value = 0.0;
				iter++;
			}
		}
	}
}

Sparse::~Sparse()
{
	delete[] m;
}

double Sparse::getValue(int row, int col)
{
	for (int iter = 0; iter < elements_count; iter++)
	{
		if (m[iter].i == row && m[iter].j == col)
			return m[iter].value;
	}
	ErrorHandler::handle_error("No value found");
	return 0.0;
}

void Sparse::setValue(int row, int col, double _value)
{
	for (int iter = 0; iter < elements_count; iter++)
	{
		if (m[iter].i == row && m[iter].j == col)
		{
			m[iter].value = _value;
			return;
		}
	}
	ErrorHandler::handle_error("No value found");
}

void Sparse::print()
{
	for (int i = 0; i < source_matrix_size; i++)
	{
		for (int j = 0; j < source_matrix_size; j++)
		{
			if (i == j || i - j == 1 || j - i == 1)
				std::cout << "| " << getValue(i, j) << " ";
			else
				std::cout << "| " << 0 << " ";
		}
		std::cout << " |" << std::endl;
	}
}
