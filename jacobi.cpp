#include "jacobi.h"

MyVector<double> * Jacobi::jacobi(MyMatrix<double>& a, MyVector<double>& b, int iter)
{
	MyVector<double> * ret_x = new MyVector<double>(b.size);
	for (int i = 0; i < iter; i++)
	{
		MyVector<double> x_copy = *ret_x;
		for (int w = 0; w < ret_x->size; w++)
		{
			ret_x->values[w] = b.values[w];
			for (int k = 0; k < ret_x->size; k++)
			{
				if (w != k)
					ret_x->values[w] -= x_copy.values[k] * a.m[w][k];
			}
			ret_x->values[w] /= a.m[w][w];
		}
	}
	return ret_x;
}

MyVector<double> * Jacobi::jacobiSprase(Sparse &a, MyVector<double> &b, int iter)
{
	MyVector<double> * ret_x = new MyVector<double>(b.size);
	for (int i = 0; i<iter; i++)
	{
		MyVector<double> x_copy = *ret_x;
		for (int w = 0; w < ret_x->size; w++) {
			ret_x->values[w] = b.values[w];
		}

		for (int k = 0; k < a.elements_count; k++)
		{
			if (a.m[k].i != a.m[k].j)
				ret_x->values[a.m[k].i] -= x_copy.values[a.m[k].j] * a.m[k].value;
		}

		for (int k = 0; k < a.elements_count; k++)
		{
			if (a.m[k].i == a.m[k].j)
				ret_x->values[a.m[k].i] /= a.m[k].value;
		}
	}
	return ret_x;
}