#pragma once
#include "myMatrix.h"
#include "myVector.h"
class GaussCalculation
{
private:
	//Returns Null if the matrix is singular
	template <class T>
	static T * calculateM(int stair_row, MyMatrix<T>* mtx, T * m_to_change, MyVector<T>* v_to_change);

	template <class T>
	static void substractForAllRows(int stair_row, MyMatrix<T>* mtx, MyVector<T> * vect, T * m);

	template <class T>
	static MyVector<T> * calculateFinalVector(MyMatrix<T>* mtx, MyVector<T> * vect);

	template <class T>
	static MyVector<T> * calculateFinalVectorByOrder(MyMatrix<T>* mtx, MyVector<T> * vect, int * order);

public:
	//Returns Null if the matrix is singular or if matrix isn't square
	template <class T>
	static MyVector<T> * gaussNoElement(MyMatrix<T> * mtrx, MyVector<T> * vect, MyVector<T> * dest = NULL);

	//Returns Null if the matrix is singular or if matrix isn't square
	template <class T>
	static MyVector<T> * gaussPartiallyChosenElement(MyMatrix<T> * mtrx, MyVector<T> * vect, MyVector<T> * dest = NULL);

	//Returns Null if the matrix is singular or if matrix isn't square
	template <class T>
	static MyVector<T> * gaussFullyChosenElement(MyMatrix<T> * mtrx, MyVector<T> * vect, MyVector<T> * dest = NULL);
};

template<class T>
MyVector<T> * GaussCalculation::gaussNoElement(MyMatrix<T>* mtx_original, MyVector<T>* v_original, MyVector<T> * dest)
{
	if (mtx_original->sizeY != v_original->size || mtx_original->sizeY != mtx_original->sizeX)
		return NULL;

	MyMatrix<T> mtx_copy = *mtx_original;
	MyVector<T> v_copy = *v_original;

	MyMatrix<T> * mtx = &mtx_copy;
	MyVector<T> * v = &v_copy;

	T * m = new T[v->size];

	//stairs
	for (int stair_row = 0; stair_row < v->size - 1; stair_row++)
	{
		calculateM(stair_row, mtx, m, v);
		if (m == NULL)
			return NULL;

		substractForAllRows(stair_row, mtx, v, m);
	}

	//!!!!!!!!!!!!!!!!!!!!!
	//delete []  m;

	MyVector<T> * ret = calculateFinalVector(mtx, v);

	if (dest != NULL) dest = ret;

	return ret;
}

template<class T>
MyVector<T> * GaussCalculation::gaussPartiallyChosenElement(MyMatrix<T>* mtx_original, MyVector<T>* v_original, MyVector<T> * dest)
{
	if (mtx_original->sizeY != v_original->size || mtx_original->sizeY != mtx_original->sizeX)
		return NULL;

	MyMatrix<T> mtx_copy = *mtx_original;
	MyVector<T> v_copy = *v_original;

	MyMatrix<T> * mtx = &mtx_copy;
	MyVector<T> * v = &v_copy;

	T * m = new T[v->size];

	//stairs
	for (int stair_row = 0; stair_row < v->size - 1; stair_row++)
	{
		int max_row;
		if (mtx->getAbsoluteMaxElementAndItsIndexes(stair_row, stair_row, &max_row, NULL) == (T)0)
		{
			return NULL;
		}

		mtx->swapRows(stair_row, max_row);
		v->swap(stair_row, max_row);

		m = calculateM(stair_row, mtx, m, v);
		if (m == NULL)
		{
			return NULL;
		}
		substractForAllRows(stair_row, mtx, v, m);
	}
	delete[] m;

	MyVector<T> * ret = calculateFinalVector(mtx, v);

	if (dest != NULL) dest = ret;

	return ret;
}

//Returns Null if the matrix is singular
template<class T>
MyVector<T> * GaussCalculation::gaussFullyChosenElement(MyMatrix<T>* mtx_original, MyVector<T>* v_original, MyVector<T> * dest)
{
	if (mtx_original->sizeY != v_original->size || mtx_original->sizeY != mtx_original->sizeX)
		return NULL;

	MyMatrix<T> mtx_copy = *mtx_original;
	MyVector<T> v_copy = *v_original;

	MyMatrix<T> * mtx = &mtx_copy;
	MyVector<T> * v = &v_copy;

	int * order = new int[v->size];
	for (int i = 0; i < v->size; i++)
	{
		order[i] = i;
	}

	T * m = new T[v->size];

	//stairs
	for (int stair_row = 0; stair_row < v->size - 1; stair_row++)
	{
		int max_row;
		int max_column;
		if (mtx->getAbsoluteMaxElementAndItsIndexes(stair_row, stair_row, &max_row, &max_column) == (T)0)
		{
			return NULL;
		}

		if (max_row != stair_row)
		{
			mtx->swapRows(stair_row, max_row);
			v->swap(stair_row, max_row);
		}

		if (max_column != stair_row)
		{
			int temp = order[stair_row];
			order[stair_row] = order[max_column];
			order[max_column] = temp;
			mtx->swapColumns(stair_row, max_column);
		}

		m = calculateM(stair_row, mtx, m, v);
		if (m == NULL)
		{
			return NULL;
		}
		substractForAllRows(stair_row, mtx, v, m);
	}
	delete[] m;

	MyVector<T> * ret = calculateFinalVectorByOrder(mtx, v, order);

	if (dest != NULL) dest = ret;
	delete[] order;
	return ret;
}

template<class T>
T * GaussCalculation::calculateM(int stair_row, MyMatrix<T>* mtx, T * m, MyVector<T>* v)
{
	for (int row = stair_row + 1; row < mtx->sizeY; row++)
	{
		//check if the m[stair_row][stair_row] == 0
		if (mtx->m[stair_row][stair_row] == (T)0)
		{
			bool found = false;
			for (int row_to_find = row; row_to_find < mtx->sizeY; row_to_find++)
			{

				if (mtx->m[row_to_find][stair_row] != (T)0)
				{
					mtx->swapRows(row_to_find, stair_row);
					v->swap(row_to_find, stair_row);
					found = true;
				}
			}
			if (!found)
			{
				if (m != NULL)
				{
					delete[] m;
					m = NULL;
				}

				return NULL;
			}
		}
		m[row] = (mtx->m[row][stair_row]) / (mtx->m[stair_row][stair_row]);
	}
	return m;
}

template<class T>
void GaussCalculation::substractForAllRows(int stair_row, MyMatrix<T>* mtx, MyVector<T> * v, T * m)
{

	//substract (m[row] * stair row) for all rows
	for (int row = stair_row + 1; row < mtx->sizeY; row++)
	{
		//set 1st column to 0s
		mtx->m[row][stair_row] = 0;

		//substract from rest columns of the 'i' row
		for (int column = stair_row + 1; column < mtx->sizeY; column++)
		{
			mtx->m[row][column] = mtx->m[row][column] - (mtx->m[stair_row][column] * m[row]);
		}

		//substract form vector
		v->values[row] = v->values[row] - v->values[stair_row] * m[row];
	}

}

template<class T>
MyVector<T>* GaussCalculation::calculateFinalVector(MyMatrix<T>* mtx, MyVector<T>* v)
{
	MyVector<T> * ret = new MyVector<T>(v->size);

	//calculating the final vector:
	for (int i = v->size - 1; i >= 0; i--)
	{
		T sum;
		sum = 0;
		for (int j = i + 1; j < v->size; j++)
		{
			sum = sum + mtx->m[i][j] * ret->values[j];
		}

		ret->values[i] = (v->values[i] - sum) / mtx->m[i][i];
	}
	return ret;
}

template<class T>
MyVector<T>* GaussCalculation::calculateFinalVectorByOrder(MyMatrix<T>* mtx, MyVector<T>* v, int * order)
{
	MyVector<T> * ret = new MyVector<T>(v->size);

	//calculating the final vector:
	for (int i = v->size - 1; i >= 0; i--)
	{
		T sum;
		sum = 0;
		for (int j = i + 1; j < v->size; j++)
		{
			sum = sum + mtx->m[i][j] * ret->values[order[j]];
		}
		ret->values[order[i]] = (T)(v->values[i] - sum) / (T)mtx->m[i][i];
	}
	return ret;
}