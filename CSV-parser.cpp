#include "CSV-parser.h"

CSVParser::CSVParser()
{
	m_file = new std::fstream();
	m_file->precision(15);
}

CSVParser::~CSVParser()
{
	delete m_file;
}

bool CSVParser::open(const char * filename)
{
	m_file->open(filename, std::ios::out | std::ios::app);
	return m_file->good();
}

void CSVParser::addElement(double element)
{
	*m_file << std::fixed << element << ";";
}

void CSVParser::addElement(int element)
{
	*m_file << element << ";";
}

void CSVParser::addElement(long long element)
{
	*m_file << element << ";";
}

void CSVParser::addElement(std::string element)
{
	*m_file << element.c_str() << ";";
}

void CSVParser::addLine()
{
	*m_file << "\n";
}

void CSVParser::clear(const char * filename)
{
	m_file->open(filename, std::ofstream::out | std::ofstream::trunc);
	m_file->close();
}

void CSVParser::close()
{
	m_file->close();
}