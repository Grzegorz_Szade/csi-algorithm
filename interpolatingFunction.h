#pragma once
#include <math.h>

class InterpolatingFunction
{
public:
	double a;
	double b;
	double c;
	double d;
	double xj;

	InterpolatingFunction();
	double execute(double x);
};