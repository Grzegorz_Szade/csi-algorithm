#pragma once
#include "myMatrix.h"

template<class T>
class MyVector
{
public:
	T * values;
	int size;

	MyVector(int n);
	MyVector(const MyVector &other);
	~MyVector();

	void swap(int i, int j);
	void generateRandomVector(int size);

	void print();

	MyVector<T> & operator=(const MyVector<T> &other);

	template <class TT>
	MyVector<T> & operator=(const MyVector<TT> &other);

	template <class TT>
	friend MyVector<TT> operator *(const MyMatrix<TT> &left, const MyVector<TT> &right);
};

template<class T>
MyVector<T>::MyVector(int n) : size(n), values(NULL)
{
	values = new T[size];
	for (int i = 0; i < size; i++)
	{
		values[i] = 0;
	}
}

template<class T>
MyVector<T>::MyVector(const MyVector &other)
{
	this->size = other.size;
	this->values = new T[this->size];

	for (int i = 0; i < this->size; i++)
	{
		this->values[i] = other.values[i];
	}
}

template<class T>
MyVector<T>::~MyVector()
{
	delete[] values;
}

template<class T>
void MyVector<T>::swap(int i, int j)
{
	T temp;
	temp = values[i];
	values[i] = values[j];
	values[j] = temp;
}

template<class T>
void MyVector<T>::generateRandomVector(int _size)
{
	if (values != NULL)
	{
		delete[] values;
		values = NULL;
	}

	size = _size;

	values = new T[size];

	//initialize with random
	for (int i = 0; i < size; i++)
	{
		values[i] = MyMath::random<T>();
	}
}

template<class T>
MyVector<T>& MyVector<T>::operator=(const MyVector<T> &other)
{
	if (&other == this)
	{
		return *this;
	}
	if (size != other.size)
	{
		if (other.values != NULL)
			delete[] values;
		size = other.size;
		values = new T[size];
	}
	size = other.size;
	for (int i = 0; i < size; i++)
	{
		values[i] = other.values[i];
	}
	return *this;
}

template<class T>
template<class TT>
MyVector<T>& MyVector<T>::operator=(const MyVector<TT> &other)
{
	if ((void *)&other == (void *)this)
	{
		return *this;
	}
	if (size != other.size)
	{
		if (other.values != NULL)
			delete[] values;
		size = other.size;
		values = new T[size];
	}
	size = other.size;
	for (int i = 0; i < size; i++)
	{
		values[i] = other.values[i];
	}
	return *this;
}


template<class T>
void MyVector<T>::print()
{
	//std::cout.precision(16);
	for (int i = 0; i < size; i++)
	{
		std::cout << "| " << (T)values[i] << " |" << std::endl;
	}
}

template<class TT>
MyVector<TT> operator*(const MyMatrix<TT>& left, const MyVector<TT>& right)
{
	MyVector<TT> ret(right.size);
	for (int i = 0; i < ret.size; i++)
	{
		for (int j = 0; j < ret.size; j++)
		{
			ret.values[i] = ret.values[i] + left.m[i][j] * right.values[j];
		}
	}
	return ret;
}