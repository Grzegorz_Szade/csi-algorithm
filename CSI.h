#pragma once
#include "data.h"
#include "interpolatingFunction.h"
#include "gaussCalculation.h"
#include "enums.h"
#include "jacobi.h"
#include "sparse.h"
#include "gaussSeidel.h"
#include "Eigen\Sparse"
#include "coefficients.h"
class CSI
{
	static InterpolatingFunction * eigenCsiMethod(Data * data, int size);
public:
	//returns an array of n interpolating functions; Data has to contain n+1 elements
	static InterpolatingFunction * csiMethod(Data * data, int size, MethodEnum method, int iter = 10);
};