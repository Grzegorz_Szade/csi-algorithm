#pragma once

#include <iostream>
#include <string>

class ErrorHandler
{
	ErrorHandler() = delete;
	~ErrorHandler() = delete;
	ErrorHandler(ErrorHandler &other) = delete;

public:
	static void handle_error(std::string err);
};