#pragma once

#include <iostream>
#include <assert.h>
#include "myMath.h" // abs

//order: matrix[y][x]

template<class T>
class MyMatrix
{
public:
	T ** m;
	int sizeX;
	int sizeY;

	MyMatrix(int n);
	MyMatrix(int x, int y);
	MyMatrix(const MyMatrix<T> &other);
	~MyMatrix();

	MyMatrix<T> & operator=(const MyMatrix<T> &other);

	template<class TT>
	MyMatrix<T> & operator=(const MyMatrix<TT> &other);

	T getAbsoluteMaxElementAndItsIndexes(int index_of_rows_to_search_from, int index_of_columns_to_search_from, int * row_index_dest, int * column_index_dest);
	void swapRows(int i, int j);
	void swapColumns(int i, int j);
	void print();
	void transpose();
	void makeThisIdentityMatrix(int size);

	//Overwrites current matrix with new with random numbers and given size by user
	void generateRandomMatrix(int size);
	void generateRandomMatrix(int sizex, int sizey, T from, T to);

	//^ and gives random between 0 and 1
	void generateRandomMatrix(int sizex, int sizey);

	MyMatrix<T> & operator *=(const MyMatrix<T> &right);
	MyMatrix<T> & operator +=(const MyMatrix<T> &right);
	MyMatrix<T> & operator *=(T right);

	template<class TT>
	friend MyMatrix<TT> operator*(MyMatrix<TT> left, const MyMatrix<TT>& right);

	template<class TT>
	friend MyMatrix<TT> operator+(MyMatrix<TT> left, const MyMatrix<TT>& right);

	template<class TT>
	friend MyMatrix<TT> operator*(TT left, MyMatrix<TT> right);

	template<class TT>
	friend MyMatrix<TT> operator*(MyMatrix<TT> left, TT right);
};

template<class T>
MyMatrix<T>::MyMatrix(int n)
{
	m = new T*[n];
	sizeY = n;
	sizeX = n;
	//Allocate space for matrix
	for (int i = 0; i < n; i++)
	{
		m[i] = new T[n];

		//initialize with 0
		for (int j = 0; j < n; j++)
		{
			m[i][j] = 0;
		}
	}
}

template<class T>
MyMatrix<T>::MyMatrix(int x, int y)
{
	m = new T*[x];
	sizeY = y;
	sizeX = x;
	//Allocate space for matrix
	for (int i = 0; i < x; i++)
	{
		m[i] = new T[y];

		//initialize with 0
		for (int j = 0; j < y; j++)
		{
			m[i][j] = 0;
		}
	}
}

template<class T>
MyMatrix<T>::MyMatrix(const MyMatrix<T>& other)
{
	sizeY = other.sizeY;
	sizeX = other.sizeX;
	if(m == NULL)
	m = new T*[sizeX];

	for (int i = 0; i < sizeX; i++)
	{
		m[i] = new T[sizeY];

		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] = other.m[i][j];
		}
	}
}

template<class T>
T MyMatrix<T>::getAbsoluteMaxElementAndItsIndexes(int index_of_rows_to_search_from, int index_of_columns_to_search_from, int * row_index_dest, int * column_index_dest)
{
	T max;
	max = m[index_of_rows_to_search_from][index_of_columns_to_search_from];
	int row_of_max = index_of_rows_to_search_from;
	int column_of_max = index_of_columns_to_search_from;
	for (int i = index_of_rows_to_search_from; i < sizeY; i++)
	{
		for (int j = index_of_columns_to_search_from; j < sizeY; j++)
		{
			if (MyMath::absolute<T>(m[i][j]) > max)
			{
				max = MyMath::absolute<T>(m[i][j]);
				row_of_max = i;
				column_of_max = j;
			}

		}
	}
	if (row_index_dest != NULL) *row_index_dest = row_of_max;
	if (column_index_dest != NULL) *column_index_dest = column_of_max;
	return max;
}

template<class T>
void MyMatrix<T>::swapRows(int i, int j)
{
	assert(!(i < 0 || j < 0 || i >= sizeY || j >= sizeY));
	T * temp = m[i];
	m[i] = m[j];
	m[j] = temp;
}

template<class T>
void MyMatrix<T>::swapColumns(int i, int j)
{
	assert(!(i < 0 || j < 0 || i >= sizeY || j >= sizeY));
	for (int row = 0; row < sizeY; row++)
	{
		T temp = m[row][i];
		m[row][i] = m[row][j];
		m[row][j] = temp;
	}
}

template<class T>
void MyMatrix<T>::print()
{
	//std::cout.precision(16);
	for (int i = 0; i < sizeX; i++)
	{
		for (int j = 0; j < sizeY; j++)
		{
			std::cout << "| " << m[i][j] << " ";
		}
		std::cout << "|" << std::endl;
	}
}

template<class T>
void MyMatrix<T>::transpose()
{
	T ** new_m = NULL;
	new_m = new T*[sizeY];
	for (int i = 0; i < sizeY; i++)
	{
		new_m[i] = new T[sizeX];
		for (int j = 0; j < sizeX; j++)
		{
			new_m[i][j] = m[j][i];
		}
	}

	for (int i = 0; i < sizeX; i++)
	{
		delete[] m[i];
		m[i] = NULL;
	}
	delete[] m;
	m = NULL;
	m = new_m;

	int temp = sizeY;
	sizeY = sizeX;
	sizeX = temp;
}

template<class T>
inline void MyMatrix<T>::makeThisIdentityMatrix(int size)
{
	if (m != NULL)
	{
		for (int i = 0; i < sizeX; i++)
		{
			delete[] m[i];
			m[i] = NULL;
		}
		delete[] m;
		m = NULL;
	}
	sizeY = size;
	sizeX = size;
	m = new T*[size];
	for (int i = 0; i < size; i++)
	{
		m[i] = new T[size];
		for (int j = 0; j < size; j++)
		{
			if (j == i)
				m[i][j] = 1;
			else
				m[i][j] = 0;
		}
	}
}

template<class T>
void MyMatrix<T>::generateRandomMatrix(int _size)
{
	if (m != NULL)
	{
		for (int i = 0; i < sizeX; i++)
		{
			delete[] m[i];
			m[i] = NULL;
		}
		delete[] m;
		m = NULL;
	}

	sizeY = _size;
	sizeX = _size;

	m = new T*[sizeX];

	for (int i = 0; i < sizeX; i++)
	{
		m[i] = new T[sizeY];

		//initialize with random
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] = MyMath::random<T>();
		}
	}
}

template<class T>
void MyMatrix<T>::generateRandomMatrix(int sizex, int sizey, T from, T to)
{
	if (m != NULL)
	{
		for (int i = 0; i < sizeX; i++)
		{
			delete[] m[i];
			m[i] = NULL;
		}
		delete[] m;
		m = NULL;
	}

	sizeY = sizey;
	sizeX = sizex;

	m = new T*[sizeX];

	for (int i = 0; i < sizeX; i++)
	{
		m[i] = new T[sizeY];

		//initialize with random
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] = MyMath::random<T>(from, to);
		}
	}
}

template<class T>
MyMatrix<T>::~MyMatrix()
{
	for (int i = 0; i < sizeX; i++)
	{
		delete[] m[i];
	}
	delete[] m;
}

template<class T>
template<class TT>
MyMatrix<T>& MyMatrix<T>::operator=(const MyMatrix<TT>& other)
{
	if ((void *)&other == (void *)this)
	{
		return *this;
	}


		if (m != NULL)
		{
			for (int i = 0; i < sizeX; i++)
			{
				delete[] m[i];
				m[i] = NULL;
			}
			delete[] m;
			m = NULL;
		}
		sizeY = other.sizeY;
		sizeX = other.sizeX;
		m = new T*[sizeX];

	for (int i = 0; i < sizeX; i++)
	{
		//????
		m[i] = new T[sizeY];
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] = other.m[i][j];
		}
	}
	return *this;
}


template<class T>
MyMatrix<T>& MyMatrix<T>::operator=(const MyMatrix<T>& other)
{
	if ((void *)&other == (void *)this)
	{
		return *this;
	}

		if (m != NULL)
		{
			for (int i = 0; i < sizeX; i++)
			{
				delete[] m[i];
				m[i] = NULL;
			}
			delete[] m;
			m = NULL;
		}
		sizeY = other.sizeY;
		sizeX = other.sizeX;
		m = new T*[sizeX];

	for (int i = 0; i < sizeX; i++)
	{
		//????
		m[i] = new T[sizeY];
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] = other.m[i][j];
		}
	}
	return *this;
}

template<class T>
void MyMatrix<T>::generateRandomMatrix(int sizex, int sizey)
{
	if (m != NULL)
	{
		for (int i = 0; i < sizeX; i++)
		{
			delete[] m[i];
			m[i] = NULL;
		}
		delete[] m;
		m = NULL;
	}

	sizeY = sizey;
	sizeX = sizex;

	m = new T*[sizeX];

	for (int i = 0; i < sizeX; i++)
	{
		m[i] = new T[sizeY];

		//initialize with random
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] = MyMath::random1<T>();
		}
	}
}

template<class T>
MyMatrix<T>& MyMatrix<T>::operator*=(const MyMatrix<T>& right)
{
	assert(sizeY == right.sizeX);

	T ** ret_m = NULL;
	ret_m = new T*[sizeX];
	for (int i = 0; i < sizeX; i++)
	{
		ret_m[i] = new T[right.sizeY];
		for (int j = 0; j < right.sizeY; j++)
		{
			ret_m[i][j] = 0;
		}
	}

	T sum;
	//calculate the multiplication
	for (int i = 0; i < sizeX; i++)
	{
		for (int j = 0; j < right.sizeY; j++)
		{
			sum = 0;
			for(int k = 0; k < sizeY; k++)
			{
				sum += m[i][k] * right.m[k][j];
			}
			ret_m[i][j] = sum;
		}
	}

	//delete *this matrix
	for (int i = 0; i < sizeX; i++)
	{
		delete [] m[i];
	}
	delete [] m;

	sizeY = right.sizeY;
	m = ret_m;
	return *this;
}

template<class T>
inline MyMatrix<T>& MyMatrix<T>::operator+=(const MyMatrix<T>& right)
{
	assert(sizeY == right.sizeY && sizeX == right.sizeX);
	for (int i = 0; i < sizeX; i++)
	{
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] += right.m[i][j];
		}
	}
	return *this;
}

template<class T>
inline MyMatrix<T>& MyMatrix<T>::operator*=(T right)
{
	for (int i = 0; i < sizeX; i++)
	{
		for (int j = 0; j < sizeY; j++)
		{
			m[i][j] *= right;
		}
	}
	return *this;
}

template<class TT>
MyMatrix<TT> operator*(MyMatrix<TT> left, const MyMatrix<TT>& right)
{
	return left *= right;
}

template<class TT>
MyMatrix<TT> operator*(TT left, MyMatrix<TT> right)
{
	return right *= left;
}

template<class TT>
MyMatrix<TT> operator*(MyMatrix<TT> left, TT right)
{
	return left *= right;
}

template <class TT>
MyMatrix<TT> operator +(MyMatrix<TT> left, const MyMatrix<TT> & right)
{
	return left += right;
}