#pragma once
#include "data.h"
#include <fstream>
#include <string.h>
#include <iostream>
#include <assert.h>
class MapsAPIParser
{
	static int countData(const char * filename, int every);
public:
	static Data * loadData(const char * filename, int every, int * nodes_count_dest);
	static Data * loadData(const char * filename, int every, int * nodes_count_dest, int firstXnodes);
};