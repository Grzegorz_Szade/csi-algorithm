#include "coefficients.h"

std::vector<Eigen::Triplet<double>>* Coefficients::getVector()
{
	return &coefficients;
}

void Coefficients::addElement(int i, int j, double value)
{
	Eigen::Triplet<double> element(i, j, value);
	coefficients.push_back(element);
}
